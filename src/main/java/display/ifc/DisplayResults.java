package display.ifc;

import java.util.Map;
import java.util.Queue;

/**
 * Created by spayker on 06.09.17.
 */
public interface DisplayResults {

    boolean display(Map<Integer, Queue<String>> wordChainMap, int shortestChainIndex, int shortestWordChain);

}
