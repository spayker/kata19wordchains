package algorithm;

import display.ConsoleDisplay;
import display.ifc.DisplayResults;

import java.util.*;

/**
 * Algorithm implementation class
 * Created by spayker on 05.09.17.
 */
public class WordChainFinder {

    private static Map<String, String> parentMap = new HashMap<>();
    private static Map<Integer, Queue<String>> wordChainMap = new HashMap<>();
    private static Map<String, Integer> visitedMap = new HashMap<>();
    private static Queue<String> wordQuery = new LinkedList<>();

    private static int wordChainCount = 0;
    private static int shortestWordChain = Integer.MAX_VALUE;
    private static int shortestChainIndex;

    /**
     * Performs search of shortest chain between first and second words
     * @param words taken from input by user
     * @param dictionaryList taken from file/URL
     * @return true if algorithm completed its work and results are shown, otherwise false
     */
    public boolean wordChainFinder(String [] words, List<String> dictionaryList) {
        if (words == null || words.length == 0) { return false; }
        if (dictionaryList == null || dictionaryList.isEmpty()) { return false; }
        String start = words[0];
        String end = words[1];

        wordQuery.add(start);
        while (!wordQuery.isEmpty()) {
            String first = wordQuery.remove();
            if (visitedMap.containsKey(first)) { continue; }
            Iterator<String> it = dictionaryList.iterator();
            while (it.hasNext()) {
                String second = it.next();
                if (!visitedMap.containsKey(second) && first.length() == second.length()) {
                    boolean isPath = isOneCharacterApart(first, second);
                    if (isPath) {
                        wordQuery.add(second);
                        parentMap.put(second, first);
                        if (second.compareTo(end) == 0) { storeValidWordChain(second); }
                    }
                }
            }
            visitedMap.put(first, 1);
        }

        // display results
        DisplayResults resultDisplayer = new ConsoleDisplay();
        resultDisplayer.display(wordChainMap, shortestChainIndex, shortestWordChain);
        return true;
    }

    /**
     * stores result with shortest word chain
     * @param target chain
     * @return true if word chain has been stored in map, otherwise returns false
     */
    public boolean storeValidWordChain(String target) {
        if(target != null && !target.isEmpty()){
            wordChainCount++;
            Queue<String> queue = new LinkedList<>();
            String word = "";
            queue.add(target);
            while ((word = parentMap.get(target)) != null) {
                target = word;
                queue.add(target);
            }
            int chainLength = queue.size();
            if (chainLength < shortestWordChain) {
                shortestWordChain = chainLength;
                shortestChainIndex = wordChainCount;
            }
            wordChainMap.put(wordChainCount, queue);
            return true;
        }
        return false;
    }

    /**
     * Compares first and second words in chain in order to define difference in characters
     * @param first word
     * @param second word
     * @return true if words are different or false if no difference was found. Returns null if length of words is different and all other scenarios
     */
    public Boolean isOneCharacterApart(String first, String second) {
        if(first != null && second != null){
            if (!first.isEmpty() && !second.isEmpty()){
                if(first.length() == second.length()) {
                    int len = first.length();
                    int diff = 0;
                    for (int i = 0; i < len; i++) {
                        if (first.charAt(i) != second.charAt(i)) {
                            diff++;
                        }
                    }
                    return (diff == 1) ? true : false;
                }
            }
        }
        return null;
    }
}
