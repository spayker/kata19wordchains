package util.dictionary;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by spayker on 05.09.17.
 */
public class FileDictionaryReaderTest {

    private FileDictionaryReader fileDictionaryReader;


    @Before
    public void init(){
        fileDictionaryReader = new FileDictionaryReader();
    }

    @Test
    public void setWrongFileAndReadDictionaryListTest(){
        assertNull(fileDictionaryReader.getFileFromApplicationProperties(null));
        assertNull(fileDictionaryReader.getFileFromApplicationProperties(""));
        assertNull(fileDictionaryReader.getFileFromApplicationProperties("wrongName"));
    }

    @Test
    public void readDictionaryListFromFileTest(){
        List<String> list = fileDictionaryReader.readDictionary();
        assertFalse(list == null);
        assertFalse(list.isEmpty());
    }

}
