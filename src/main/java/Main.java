import algorithm.WordChainFinder;
import util.dictionary.FileDictionaryReader;
import util.dictionary.NetDictionaryReader;
import util.dictionary.ifc.DictionaryReader;
import java.io.*;
import java.util.*;

/**
 * Main class launcher to run and play with algorithm
 * Created by spayker on 05.09.17.
 */
public class Main {

    public static void main(String[] args) {
        // init data
        DictionaryReader dictionaryReader = new NetDictionaryReader(); // like an alternative way to get dictionary we can use net dictionary reader can be used
        List<String> dictionaryList = dictionaryReader.readDictionary();

        // get first and second word from user's input
        String[] inputWords = readInput();

        // run main algorithm
        WordChainFinder finder = new WordChainFinder();
        finder.wordChainFinder(inputWords, dictionaryList);
    }

    /**
     * reads input by user
     * @return String array with set of words (usually it's: first and second words)
     */
    public static String [] readInput() {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Type first word and second one to run algorithm (third and further words won't be taken): ");
            String [] inputStrs = bufferedReader.readLine().split(" ");
            return inputStrs;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
