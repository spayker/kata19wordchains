package util.dictionary;

import util.dictionary.ifc.DictionaryReader;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Implementation class of dictionary reader from file
 * Created by spayker on 05.09.17.
 */
public class FileDictionaryReader implements DictionaryReader {

    private final String DICTIONARY_FILE_PROPERTY = "dictionaryFileName";

    /**
     * reads dictionary list from file
     * @return List<String> of words taken from dictionary file otherwise null
     */
    public List<String> readDictionary() {
        File file = getFileFromApplicationProperties(DICTIONARY_FILE_PROPERTY);

        if(file != null){
            List<String> dictionaryList = new LinkedList<>();
            try (Stream<String> stream = Files.lines(Paths.get(file.getPath()))) {
                stream.forEach((v) -> {
                    String [] sArr = v.split(" ");
                    int len = sArr.length;
                    for (int i=0; i<len; i++) {
                        dictionaryList.add(sArr[i]);
                    }
                });
                return dictionaryList;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Returns file with dictionary according to application properties
     * @param dictionaryFileProperty name which contains file name
     * @return File instance with dictionary otherwise null
     */
    public File getFileFromApplicationProperties(String dictionaryFileProperty){
        if(dictionaryFileProperty != null && !dictionaryFileProperty.isEmpty()){
            ClassLoader classLoader = getClass().getClassLoader();

            String propertyValue = getPropertyValue(dictionaryFileProperty);
            if(propertyValue != null){
                String filePath = classLoader.getResource(propertyValue).getFile();
                if(filePath != null){
                    return new File(filePath);
                }
            }
        }
        return null;
    }
}
