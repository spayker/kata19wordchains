package algorithm;

import org.junit.Before;
import org.junit.Test;
import util.dictionary.FileDictionaryReader;
import util.dictionary.ifc.DictionaryReader;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

/**
 * Created by spayker on 05.09.17.
 */
public class WordChainFinderTest {

    private WordChainFinder wordChainFinder;

    @Before
    public void init(){
        wordChainFinder = new WordChainFinder();
    }

    @Test
    public void setProperValuesForWordChainFinder(){
        // init data
        DictionaryReader dictionaryReader = new FileDictionaryReader();
        List<String> dictionaryList = dictionaryReader.readDictionary();

        // get first and second word from user's input
        String[] inputWords = new String[]{"cat","dog"};

        // run main algorithm
        WordChainFinder finder = new WordChainFinder();
        assertTrue(finder.wordChainFinder(inputWords, dictionaryList));
    }

    @Test
    public void setNullInputParametersForWordChainFinder(){
        assertFalse(wordChainFinder.wordChainFinder(null, null));
        assertFalse(wordChainFinder.wordChainFinder(new String[]{"cat","dog"}, null));
        assertFalse(wordChainFinder.wordChainFinder(null, new LinkedList<>(Arrays.asList("cat","cig","cir"))));
    }

    @Test
    public void setNullOrEmptyValueForStoreValidWordChain(){
        assertFalse(wordChainFinder.storeValidWordChain(null));
        assertFalse(wordChainFinder.storeValidWordChain(""));
    }

    @Test
    public void setWordsWithDifferentLengthForCheckWordDifferenceTest(){
        assertNull(wordChainFinder.isOneCharacterApart("cat", "dogg"));
    }

    @Test
    public void setNullValuesForCheckWordDifferenceTest(){
        assertNull(wordChainFinder.isOneCharacterApart(null, null));
        assertNull(wordChainFinder.isOneCharacterApart("cat", null));
        assertNull(wordChainFinder.isOneCharacterApart(null, "cat"));
    }

    @Test
    public void setEmptyStringsForCheckWordDifferenceTest(){
        assertNull(wordChainFinder.isOneCharacterApart("", ""));
        assertNull(wordChainFinder.isOneCharacterApart("cat", ""));
        assertNull(wordChainFinder.isOneCharacterApart("", "cat"));
    }

}
