package display;

import display.ifc.DisplayResults;

import java.util.Comparator;
import java.util.Map;
import java.util.Queue;

/**
 * Implements way to show results in console
 * Created by spayker on 05.09.17.
 */
public class ConsoleDisplay implements DisplayResults {

    /**
     * Displays results in console
     * @param wordChainMap shortest word's chain map
     * @param shortestChainIndex defines location in map with shortest word's chain
     * @param shortestWordChain amount of words in shortest chain
     * @return true if results has been displayed, otherwise false
     */
    public boolean display(Map<Integer, Queue<String>> wordChainMap, int shortestChainIndex, int shortestWordChain) {
        if(shortestWordChain > 0) {
            System.out.println("Length of shortest word chain: " + shortestWordChain);
            Queue<String> queue = wordChainMap.get(shortestChainIndex);

            if (queue == null) {
                return false;
            }
            queue.stream().sorted(Comparator.naturalOrder()).forEach((v) -> System.out.println(v));
            return true;
        }
        return false;
    }

}
