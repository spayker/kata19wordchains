package util.dictionary.ifc;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Created by spayker on 05.09.17.
 */
public interface DictionaryReader {

    String APPLICATION_PROPERTIES_FILE_NAME = "application.properties";

    List<String> readDictionary();

    default String getPropertyValue(String propertyToRead){
        if(propertyToRead != null && !propertyToRead.isEmpty()){
            Properties prop = new Properties();
            try {
                //load a properties file from class path, inside static method
                prop.load(getClass().getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES_FILE_NAME));

                //get the property value and return it
                return prop.getProperty(propertyToRead);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

}
