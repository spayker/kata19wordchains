# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A quick implementation of Kata19: Word Chains task
* 1.0

### How do I get set up? ###

* Summary of set up:
1) create maven project in IDEA
2) copy all fodlers from src folder to same one in new project
3) copy pom.xml to same path in new project
* Configuration
1) project contains application.properties file to configure dictionary locations
* Dependencies
1) junit 4
* Database configuration
2) none
* How to run tests
1) in idea open any test class in test fodler and press "run "name of current test class""
* Deployment instructions
1) project was assembled in jar file. So it can be run easily with the following command in console:
java -jar Kata19WordChains-1.0-SNAPSHOT.jar