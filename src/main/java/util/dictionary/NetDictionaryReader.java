package util.dictionary;

import util.dictionary.ifc.DictionaryReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation class of dictionary reader from URL (url is taken from application.properties)
 * Created by spayker on 05.09.17.
 */
public class NetDictionaryReader implements DictionaryReader{

    private final String DICTIONARY_URL_PROPERTY = "dictionaryURL";

    /**
     * reads dictionary list from URL
     * @return List<String> of words taken from dictionary url address
     */
    public List<String> readDictionary() {
        List<String> dictionaryList = new LinkedList<>();
        try {
            URL url = new URL(getPropertyValue(DICTIONARY_URL_PROPERTY));
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                dictionaryList.add(inputLine);
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dictionaryList;
    }
}
