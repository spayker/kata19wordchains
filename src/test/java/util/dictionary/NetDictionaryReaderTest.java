package util.dictionary;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

/**
 * Created by spayker on 05.09.17.
 */
public class NetDictionaryReaderTest {

    private NetDictionaryReader netDictionaryReader;

    @Before
    public void init(){
        netDictionaryReader = new NetDictionaryReader();
    }

    @Test
    public void setWrongFileAndReadDictionaryListTest(){
        assertNull(netDictionaryReader.getPropertyValue(null));
        assertNull(netDictionaryReader.getPropertyValue(""));
        assertNull(netDictionaryReader.getPropertyValue("wrongName"));
    }

    @Test
    public void readDictionaryListFromFileTest(){
        List<String> list = netDictionaryReader.readDictionary();
        assertFalse(list == null);
        assertFalse(list.isEmpty());
    }

}
